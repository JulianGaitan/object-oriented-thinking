package org.example.file;

import org.example.person.CardID;

import java.sql.Connection;

public class UserDB {

    private Connection sqlConnection;

    public UserDB(Connection sqlConnection) {
        this.sqlConnection = sqlConnection;
    }

    public boolean verifyCardID(CardID cardId) {
        // true if the card ID is valid, false otherwise
        return true;
    }

    public boolean checkAuthorization(CardID cardId) {
        // true if the card ID is authorized, false otherwise
        return true;
    }
}
