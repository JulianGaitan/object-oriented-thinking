package org.example.file;

import java.nio.file.Path;

public class File {

    private byte[] data;
    private String name;
    private String extension;
    private Path path;

    public File(byte[] data, Path path) {
        this.data = data;
        this.path = path;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public String getName() {
        // get name from path
        return "name";
    }

    public String getExtension() {
        // get extension from path
        return "extension";
    }

    public boolean checkIfValidFile() {
        // return true if the file is valid, false otherwise
        return true;
    }
}
