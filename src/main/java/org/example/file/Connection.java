package org.example.file;

import java.nio.file.Path;

public class Connection {

    private String connectionType;
    private String deviceName;

    public Connection(String connectionType, String deviceName) {
        this.connectionType = connectionType;
        this.deviceName = deviceName;
    }

    public boolean openConnection(String type, String device) {
        // open connection: return true if successful, false otherwise
        return true;
    }

    public boolean closeConnection() {
        // close connection: return true if successful, false otherwise
        return true;
    }

    public File downloadFile(Path path) {
        // download file
        return new File(new byte[0], Path.of(""));
    }

    public void uploadFile(Path path, File file) {
        // upload file
    }
}
