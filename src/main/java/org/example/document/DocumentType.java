package org.example.document;

public enum DocumentType {
    PRINT,
    SCAN,
    PHOTOCOPY
}
