package org.example.document;

import org.example.file.File;

import javax.swing.*;
import java.awt.*;

public class Document {

    private File file;
    private DocumentType docType;
    private int height;
    private int width;
    private int margin;
    private int color;

    public Document(File file, DocumentType docType) {
        this.file = file;
        this.docType = docType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Image showPreview() {
        // construct an image from the file data and document's attributes
        return new ImageIcon("").getImage();
    }
}
