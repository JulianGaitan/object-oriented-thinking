package org.example.printer;

import org.example.document.Document;
import org.example.document.DocumentType;
import org.example.file.Connection;
import org.example.file.File;
import org.example.file.UserDB;

import java.nio.file.Path;

public class Printer {

    public class Paper {

        private int paperCount;

        public Paper(int paperCount) {
            this.paperCount = paperCount;
        }

        public int getPaperCount() {
            return paperCount;
        }

        public void setPaperCount(int paperCount) {
            this.paperCount = paperCount;
        }
    }

    public class InkCartridge {

        private int inkLevel;
        private int inkColor;

        public InkCartridge(int inkLevel, int inkColor) {
            this.inkLevel = inkLevel;
            this.inkColor = inkColor;
        }

        public int getInkLevel() {
            return inkLevel;
        }

        public void setInkLevel(int inkLevel) {
            this.inkLevel = inkLevel;
        }

        public int getInkColor() {
            return inkColor;
        }

        public void setInkColor(int inkColor) {
            this.inkColor = inkColor;
        }
    }

    private final Paper[] paperAvailable;
    private final InkCartridge[] inkAvailable;
    private Connection connection;
    private final UserDB userDB;

    public Printer(Paper[] paperAvailable, InkCartridge[] inkAvailable, UserDB userDB) {
        this.paperAvailable = paperAvailable;
        this.inkAvailable = inkAvailable;
        this.userDB = userDB;
    }

    public Paper getPaperAvailable(int index) {
        return paperAvailable[index];
    }

    public InkCartridge getInkAvailable(int index) {
        return inkAvailable[index];
    }

    public UserDB getUserDB() {
        return userDB;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void printDocument(Document doc) {
        // print document
    }

    public Document scanDocument() {
        // scan document
        return new Document(new File(new byte[0], Path.of("")), DocumentType.SCAN);
    }

    public void photocopyDocument() {
        // photocopy document
    }
}
