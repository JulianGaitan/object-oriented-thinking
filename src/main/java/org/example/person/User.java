package org.example.person;

public class User extends Person {

    private double balance;

    public User(CardID cardID) {
        super(cardID);
        this.balance = 0;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
