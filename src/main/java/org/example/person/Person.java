package org.example.person;

import java.util.Date;

public abstract class Person {

    private Date lastAccess;
    private CardID cardID;

    public Person(CardID cardID) {
        this.cardID = cardID;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

    public CardID getCardID() {
        return cardID;
    }

    public void setCardID(CardID cardID) {
        this.cardID = cardID;
    }

}
