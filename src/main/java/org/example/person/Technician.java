package org.example.person;

public class Technician extends Person {

    private String accessLevel;

    public Technician(CardID cardID, String accessLevel) {
        super(cardID);
        this.accessLevel = accessLevel;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

}
